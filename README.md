# CDP Kafka Setup

Designed to initialize Kafka with the desired configurations



## Development Information

Execute `python -m unittest` on the root of the repo to run testing

## Troubleshooting Information

If you have trouble getting the docker image to run, be sure to check that `run.sh` has unix file line endings and correct. This problem can exhibit itself with the following error on run:

`standard_init_linux.go:211: exec user process caused "no such file or directory"`

To fix this, make sure `run.sh` and `Dockerfile` is saved with unix line endings. This can be done with gitbash at the root of this repository:

`$ dos2unix run.sh`
`$ dos2unix Dockerfile`



